# ProteoxDriver

A QCoDeS driver for the Proteox Fridge by Oxford Instruments

## Installation

```cmd
pip install proteox_driver --index-url https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi/simple
```

## Usage

```py
from proteox_driver.proteox import oiDECS
driver = oiDECS(name="test", host="localhost", port=33577)
driver.PT1_Head_Temperature()
```
