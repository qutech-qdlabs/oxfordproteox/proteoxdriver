from typing import Any

from qcodes.instrument import VisaInstrument
from qcodes.parameters import MultiParameter

from proteox_driver.scpi import scpi_query


class MagneticFieldParameters(MultiParameter):
    """
    Parameter for retrieving X, Y, and Z components of the magnetic field.

    To Retrieve all three parameters via `instrument.Magnetic_Field_Vector()`
    """

    def __init__(self, name, instrument: VisaInstrument, **kwargs: Any) -> None:
        super().__init__(
            name=name,
            instrument=instrument,
            names=("X_Field", "Y_Field", "Z_Field"),
            labels=(
                f"{instrument} X_Field",
                f"{instrument} Y_Field",
                f"{instrument} Z_Field",
            ),
            units=("T", "T", "T"),
            setpoints=((), (), ()),
            shapes=((), (), ()),
            snapshot_get=True,
            snapshot_value=True,
            **kwargs,
        )
        self.instrument: VisaInstrument  # For type hinting only

    def get_raw(self) -> tuple[float, ...]:
        """
        Gets the values of magnetic field from the instrument
        """
        b_str = self.instrument.visa_handle.query(
            scpi_query("oi.decs.magnetic_field_control.VRM_01.magnetic_field_vector")
        )
        b_array = b_str.split(",")
        return float(b_array[0]), float(b_array[1]), float(b_array[2])

    def set_raw(self, value) -> None:
        """
        Unused overide of base class function
        """
        print("*** Field cannot be set directly with this function ***")


class MagnetCurrentParameters(MultiParameter):
    """
    Parameter for retrieving X, Y, and Z components of the magnet current.

    Retrieve all three parameters via `instrument.Magnet_Current_Vector()`
    """

    def __init__(self, name, instrument: VisaInstrument, **kwargs: Any) -> None:
        super().__init__(
            name=name,
            instrument=instrument,
            names=("X_Current", "Y_Current", "Z_Current"),
            labels=(
                f"{instrument} X_Current",
                f"{instrument} Y_Current",
                f"{instrument} Z_Current",
            ),
            units=("A", "A", "A"),
            setpoints=((), (), ()),
            shapes=((), (), ()),
            snapshot_get=True,
            snapshot_value=True,
            **kwargs,
        )
        self.instrument: VisaInstrument  # For type hinting only

    def get_raw(self) -> tuple[float, ...]:
        """
        Gets the values of magnet current from the instrument
        """
        i_str = self.instrument.visa_handle.query(scpi_query("oi.decs.magnetic_field_control.VRM_01.current_vector"))
        i_array = i_str.split(",")
        return float(i_array[0]), float(i_array[1]), float(i_array[2])

    def set_raw(self, value) -> None:
        """
        Unused overide of base class function
        """
        print("*** Current cannot be set directly with this function ***")
