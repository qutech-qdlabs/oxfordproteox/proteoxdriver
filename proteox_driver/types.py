# pylint: disable=invalid-name
from enum import IntEnum


class SystemControlMode(IntEnum):
    Local = 0
    Remote = 1


class OiDataRecordType(IntEnum):
    OiTemperatureRecord = 0
    OiPressureRecord = 10
    OiMolarFlowRecord = 20
    OiCurrentRecord = 50
    OiVoltageRecord = 60
    OiPowerRecord = 70
    OiResistanceRecord = 90
    OiValveStateRecord = 1000
    OiPumpSpeedRecord = 1010
    OiOnOffStateRecord = 1030
    OiHeaterPowerRecord = 1040
    OiControlLoopRecord = 1050
    OiUpsStateRecord = 1060
    OiPressureSwitchStateRecord = 1120
    OiCountRecord = 1200
    OiMagnetFieldVectorRecord = 1400
    OiCurrentVectorRecord = 1410
    OiProteoxStateRecord = 5000


class OiCumulativeStatus(IntEnum):
    OiCumulativeStatusNormal = 0
    OiCumulativeStatusWarning = 50
    OiCumulativeStatusError = 100


class OiProteoxMajorState(IntEnum):
    OiProteoxIdling = 0
    OiProteoxPumping = 2000
    OiProteoxCondensing = 4000
    OiProteoxCirculating = 5000
    OiProteoxWarmUp = 8000
    OiProteoxCleaningTrapCold = 100_000
    OiProteoxExchangingSample = 110_000

    def __str__(self):  # pylint: disable=too-many-return-statements
        if self == self.OiProteoxIdling:
            return "IDLE"
        elif self == self.OiProteoxPumping:
            return "PUMPING"
        elif self == self.OiProteoxCondensing:
            return "CONDENSING"
        elif self == self.OiProteoxCirculating:
            return "CIRCULATING"
        elif self == self.OiProteoxWarmUp:
            return "WARM UP"
        elif self == self.OiProteoxCleaningTrapCold:
            return "CLEAN_TRAP_COLD"
        elif self == self.OiProteoxExchangingSample:
            return "SAMPLE_EXCHANGE"
        else:
            return "UNKNOWN"


class OiProteoxDemandedState(IntEnum):
    OiStopProteox = 0
    OiCooldownProteox = 1000
    OiPumpDownProteox = 2000
    OiPreCoolProteox = 3000
    OiCondenseProteox = 4000
    OiCirculateProteox = 5000
    OiWarmUpProteox = 8000
    OiCollectMixProteox = 9000
    OiCleanTrapColdProteox = 100_000
    OiExchangeSampleProteox = 110_000

    def __str__(self):  # pylint: disable=too-many-return-statements
        if self == self.OiStopProteox:
            return "MANUAL_MODE"
        elif self == self.OiCooldownProteox:
            return "COOLDOWN"
        elif self == self.OiPumpDownProteox:
            return "PUMP_DOWN"
        elif self == self.OiPreCoolProteox:
            return "PRECOOL"
        elif self == self.OiCondenseProteox:
            return "CONDENSE"
        elif self == self.OiCirculateProteox:
            return "CIRCULATE"
        elif self == self.OiWarmUpProteox:
            return "WARM_UP"
        elif self == self.OiCollectMixProteox:
            return "COLLECT_MIX"
        elif self == self.OiCleanTrapColdProteox:
            return "CLEAN_TRAP_COLD"
        elif self == self.OiExchangeSampleProteox:
            return "EXCHANGE_SAMPLE"
        else:
            return "UNKNOWN"


class OiMagnetVectorRotateState(IntEnum):
    OiMagnetVectorRotateStateHoldNotPersistent = 0
    OiMagnetVectorRotateStateHoldPersistent = 10
    OiMagnetVectorRotateStateSweepMagneticField = 20
    OiMagnetVectorRotateStateSweepPsuOutput = 30
    OiMagnetVectorRotateStateOpeningSwitch = 40
    OiMagnetVectorRotateStateClosingSwitch = 50
    OiMagnetVectorRotateStateMagnetSafety = 60
    OiMagnetVectorRotateStateMagnetSafetyPersistent = 70

    def __str__(self):  # pylint: disable=too-many-return-statements
        if self == self.OiMagnetVectorRotateStateHoldNotPersistent:
            return "HOLDING NOT PERSISTENT"
        elif self == self.OiMagnetVectorRotateStateHoldPersistent:
            return "HOLDING PERSISTENT"
        elif self == self.OiMagnetVectorRotateStateSweepMagneticField:
            return "RAMPING FIELD"
        elif self == self.OiMagnetVectorRotateStateSweepPsuOutput:
            return "RAMPING LEADS"
        elif self == self.OiMagnetVectorRotateStateOpeningSwitch:
            return "RAMPING FIELD"
        elif self == self.OiMagnetVectorRotateStateClosingSwitch:
            return "RAMPING LEADS"
        elif self == self.OiMagnetVectorRotateStateMagnetSafety:
            return "MAGNET SAFETY"
        elif self == self.OiMagnetVectorRotateStateMagnetSafetyPersistent:
            return "MAGNET SAFETY PERSISTENT"
        else:
            return "UNKNOWN"


class OiMagnetVectorRotateDemandedState(IntEnum):
    OiMagnetVectorRotateDemandedStateHold = 0
    OiMagnetVectorRotateDemandedStateEnterPersistentMode = 10
    OiMagnetVectorRotateDemandedStateLeavePersistentMode = 20
    OiMagnetVectorRotateDemandedStateSweepMagneticField = 30
    OiMagnetVectorRotateDemandedStateSweepPsuOutput = 40

    def __str__(self):
        if self == self.OiMagnetVectorRotateDemandedStateHold:
            return "Hold"
        elif self == self.OiMagnetVectorRotateDemandedStateEnterPersistentMode:
            return "Entering Persistent Mode"
        elif self == self.OiMagnetVectorRotateDemandedStateLeavePersistentMode:
            return "Leave Persistent Mode"
        elif self == self.OiMagnetVectorRotateDemandedStateSweepMagneticField:
            return "Sweep Field"
        elif self == self.OiMagnetVectorRotateDemandedStateSweepPsuOutput:
            return "Sweep PSU Output"
        else:
            return "UNKNOWN"


class OiVectorCoordinateSystem(IntEnum):
    OiVectorCoordinateSystemCartesian = 0
    OiVectorCoordinateSystemCylindrical = 10
    OiVectorCoordinateSystemSpherical = 20

    def __str__(self):
        if self == self.OiVectorCoordinateSystemCartesian:
            return "Cartesian"
        elif self == self.OiVectorCoordinateSystemCylindrical:
            return "Cylindrical"
        elif self == self.OiVectorCoordinateSystemSpherical:
            return "Spherical"
        else:
            return "UNKNOWN"


class OiPsuSweepMode(IntEnum):
    OiPsuSweepModeASAP = 0
    OiPsuSweepModeTime = 10
    OiPsuSweepModeRate = 20

    def __str__(self):
        if self == self.OiPsuSweepModeASAP:
            return "ASAP"
        elif self == self.OiPsuSweepModeTime:
            return "Time"
        elif self == self.OiPsuSweepModeRate:
            return "Rate"
        else:
            return "UNKNOWN"


class OiControlLoopMode(IntEnum):
    OiControlLoopModeOpen = 0
    OiControlLoopModeClosed = 1


class OiUpsState(IntEnum):
    OiUpsNormal = 0
    OiUpsOnBattery = 10
    OiUpsCritical = 20


class OiMagnetSwitchState(IntEnum):
    OiMagnetSwitchClosed = 0
    OiMagnetSwitchOpen = 1

    def __str__(self):
        if self == self.OiMagnetSwitchClosed:
            return "Closed"
        elif self == self.OiMagnetSwitchOpen:
            return "Open"
        else:
            return "UNKNOWN"
