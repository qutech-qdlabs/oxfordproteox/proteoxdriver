from proteox_driver.types import OiMagnetSwitchState, OiMagnetVectorRotateState


def magnet_state_parser(val_str: str):
    try:
        val = int(val_str)
    except ValueError:
        return -1
    else:
        return OiMagnetVectorRotateState(val)


def magnet_switch_parser(val_str: str):
    try:
        val = int(val_str)
    except ValueError:
        return -1
    else:
        return OiMagnetSwitchState(val)
