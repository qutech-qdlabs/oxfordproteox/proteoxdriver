def scpi_query(cmd_uri: str) -> str:
    return f"{cmd_uri}?"


def scpi_command(cmd_uri: str, *vals) -> str:
    if len(vals) == 0:
        return cmd_uri
    else:
        return f"{cmd_uri} {','.join([str(val) for val in vals])}"
