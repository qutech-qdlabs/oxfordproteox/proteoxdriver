import time
from functools import partial

import numpy as np
from qcodes.instrument import VisaInstrument

from proteox_driver import types
from proteox_driver.io import magnet_state_parser, magnet_switch_parser
from proteox_driver.parameters import MagnetCurrentParameters, MagneticFieldParameters
from proteox_driver.scpi import scpi_command, scpi_query


class oiDECS(VisaInstrument):  # pylint: disable=invalid-name
    """Main implementation of the oi.DECS driver"""

    def __init__(  # pylint: disable=too-many-positional-arguments
        self,
        name: str,
        host: str,
        port: int,
        system_has_magnet: bool = True,
        magnet_has_switch: bool = False,
        dual_ptrs_fitted: bool = False,
        dual_turbo_fitted: bool = False,
        he3_flow_meter_fitted: bool = False,
        **kwargs,
    ):
        super().__init__(name, f"TCPIP::{host}::{port}::SOCKET", terminator="\n", **kwargs)
        # System Config
        self.system_has_magnet = system_has_magnet
        self.magnet_has_switch = magnet_has_switch
        self.dual_ptrs_fitted = dual_ptrs_fitted
        self.dual_turbo_fitted = dual_turbo_fitted
        self.he3_flow_meter_fitted = he3_flow_meter_fitted

        # Parameters
        self.add_parameter(
            "PT1_Head_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.PTR1_PT1_S.temperature"),
            get_parser=float,
        )
        self.add_parameter(
            "PT1_Plate_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_PT1_S.temperature"),
            get_parser=float,
        )
        self.add_parameter(
            "PT2_Head_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.PTR1_PT2_S.temperature"),
            get_parser=float,
        )
        self.add_parameter(
            "PT2_Plate_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_PT2_S.temperature"),
            get_parser=float,
        )
        self.add_parameter(
            "Sorb_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.SRB_GGS_CL.SRB_GGS_S.temperature"),
            get_parser=float,
        )
        self.add_parameter(
            "Still_Plate_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_STL_S.temperature"),
            get_parser=float,
        )
        self.add_parameter(
            "Still_Heater_Power",
            unit="W",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_STL_H.power"),
            set_cmd=partial(self._param_setter, "oi.decs.temperature_control.DRI_STL_H.power"),
            get_parser=float,
        )
        self.add_parameter(
            "Cold_Plate_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_CLD_S.temperature"),
            get_parser=float,
        )
        self.add_parameter(
            "Mixing_Chamber_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_MIX_CL.DRI_MIX_S.temperature"),
            set_cmd=partial(self._param_setter, "oi.decs.temperature_control.DRI_MIX_CL.setpoint"),
            get_parser=float,
        )
        self.add_parameter(
            "Mixing_Chamber_Temperature_Target",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_MIX_CL.setpoint"),
            set_cmd=partial(self._param_setter, "oi.decs.temperature_control.DRI_MIX_CL.setpoint"),
            get_parser=float,
        )
        self.add_parameter(
            "Mixing_Chamber_Heater_Power",
            unit="W",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_MIX_CL.DRI_MIX_H.power"),
            set_cmd=partial(self._param_setter, "oi.decs.temperature_control.DRI_MIX_CL.DRI_MIX_H.power"),
            get_parser=float,
        )
        self.add_parameter(
            "Sample_Temperature",
            unit="K",
            label=name,
            get_cmd=scpi_query("oi.decs.temperature_control.DRI_MIX_CL.DRI_MIX_S.temperature"),
            set_cmd=partial(self._param_setter, "oi.decs.temperature_control.DRI_MIX_CL.setpoint"),
            get_parser=float,
        )
        self.add_parameter(
            "OVC_Pressure",
            unit="Pa",
            label=name,
            get_cmd=scpi_query("oi.decs.proteox.OVC_PG_01.pressure"),
            get_parser=float,
        )
        self.add_parameter(
            "P1_Pressure",
            unit="Pa",
            label=name,
            get_cmd=scpi_query("oi.decs.proteox.3CL_PG_01.pressure"),
            get_parser=float,
        )
        self.add_parameter(
            "P2_Pressure",
            unit="Pa",
            label=name,
            get_cmd=scpi_query("oi.decs.proteox.3CL_PG_02.pressure"),
            get_parser=float,
        )
        self.add_parameter(
            "P3_Pressure",
            unit="Pa",
            label=name,
            get_cmd=scpi_query("oi.decs.proteox.3CL_PG_03.pressure"),
            get_parser=float,
        )
        self.add_parameter(
            "P4_Pressure",
            unit="Pa",
            label=name,
            get_cmd=scpi_query("oi.decs.proteox.3CL_PG_04.pressure"),
            get_parser=float,
        )
        self.add_parameter(
            "P5_Pressure",
            unit="Pa",
            label=name,
            get_cmd=scpi_query("oi.decs.proteox.3CL_PG_05.pressure"),
            get_parser=float,
        )
        self.add_parameter(
            "P6_Pressure",
            unit="Pa",
            label=name,
            get_cmd=scpi_query("oi.decs.proteox.3CL_PG_06.pressure"),
            get_parser=float,
        )

        if self.he3_flow_meter_fitted:
            self.add_parameter(
                "He3_Flow",
                unit="mol/s",
                label=name,
                get_cmd=scpi_query("oi.decs.flow_control.3CL_FM_01.flow"),
                get_parser=float,
            )

        if self.system_has_magnet:
            self.add_parameter(
                "Magnet_Temperature",
                unit="K",
                label=name,
                get_cmd=scpi_query("oi.decs.magnetic_field_control.MAG_MSP_S.temperature"),
                get_parser=float,
            )
            self.add_parameter(
                "Magnet_State",
                label=name,
                get_cmd=scpi_query("oi.decs.magnetic_field_control.VRM_01.state"),
                get_parser=magnet_state_parser,
            )
            self.add_parameter(
                name="Magnetic_Field_Vector",
                parameter_class=MagneticFieldParameters,
            )
            self.add_parameter(
                name="Magnet_Current_Vector",
                parameter_class=MagnetCurrentParameters,
            )

            if self.magnet_has_switch:
                self.add_parameter(
                    "Switch_State",
                    label=name,
                    get_cmd=scpi_query("oi.decs.magnetic_field_control.VRM_01.SWZ.state"),
                    get_parser=magnet_switch_parser,
                )

        self.connect_message()

    def connect_message(self, idn_param: str = "*IDN", begin_time: float | None = None) -> None:
        """
        Print a standard message on initial connection to an instrument.

        Args:
            idn_param: Name of parameter that returns ID dict.
                Default ``IDN``.
            begin_time: ``time.time()`` when init started.
                Default is ``self._t0``, set at start of ``Instrument.__init__``.
        """
        idn_string = self.visa_handle.query(scpi_query(idn_param), delay=begin_time)
        idn_parts = idn_string.replace(" ", "").split(",")
        idn = {"vendor": idn_parts[0], "model": idn_parts[1], "serial": idn_parts[2], "firmware": idn_parts[3]}
        t = time.time() - (begin_time or self._t0)

        con_msg = (
            "Connected to: {vendor} {model} " "(serial:{serial}, firmware:{firmware}) " "in {t:.2f}s".format(t=t, **idn)
        )
        print(con_msg)
        self.log.info(f"Connected to instrument: {idn}")

    def _param_setter(self, set_cmd: str, *vals) -> None:
        self.visa_handle.query(scpi_command(set_cmd, *vals))

    ####################
    # Helper methods
    ####################

    def set_magnet_target(  # pylint: disable=too-many-positional-arguments
        self,
        coord: types.OiVectorCoordinateSystem,
        x: float,
        y: float,
        z: float,
        sweep_mode: types.OiPsuSweepMode,
        sweep_parameter: float,
        persist_on_completion: bool,
    ):
        param = [coord.value, x, y, z, sweep_mode.value, sweep_parameter, persist_on_completion]
        self._param_setter("oi.decs.magnetic_field_control.VRM_01.set_field_target", *param)

    def set_output_current_target(  # pylint: disable=too-many-positional-arguments
        self,
        x: float,
        y: float,
        z: float,
        sweep_mode: types.OiPsuSweepMode,
        sweep_parameter: float,
        persist_on_completion: bool,
    ):
        param = [x, y, z, sweep_mode.value, sweep_parameter, persist_on_completion]
        self._param_setter("oi.decs.magnetic_field_control.VRM_01.set_output_current_target", *param)

    def mixing_chamber_heater_off(self):
        """Function to turn off MC heater"""
        self._param_setter("oi.decs.temperature_control.DRI_MIX_CL.DRI_MIX_H.power", 0)

    def still_heater_off(self):
        """Function to turn off still heater"""
        self._param_setter("oi.decs.temperature_control.DRI_STL_H.power", 0)

    def set_magnet_state(self, state: types.OiMagnetVectorRotateDemandedState):
        self._param_setter("oi.decs.magnetic_field_control.VRM_01.set_state", state.value)
        print(state)

    def sweep_field(self):
        self.set_magnet_state(
            types.OiMagnetVectorRotateDemandedState.OiMagnetVectorRotateDemandedStateSweepMagneticField
        )

    def sweep_psu_output(self):
        self.set_magnet_state(types.OiMagnetVectorRotateDemandedState.OiMagnetVectorRotateDemandedStateSweepPsuOutput)

    def enter_persistent_mode(self):
        self.set_magnet_state(
            types.OiMagnetVectorRotateDemandedState.OiMagnetVectorRotateDemandedStateEnterPersistentMode
        )

    def leave_persistent_mode(self):
        self.set_magnet_state(
            types.OiMagnetVectorRotateDemandedState.OiMagnetVectorRotateDemandedStateLeavePersistentMode
        )

    def hold_field(self):
        self.set_magnet_state(types.OiMagnetVectorRotateDemandedState.OiMagnetVectorRotateDemandedStateHold)

    def open_switch(self):
        """Does the same as `enter_persistent_mode`"""
        self.set_magnet_state(
            types.OiMagnetVectorRotateDemandedState.OiMagnetVectorRotateDemandedStateLeavePersistentMode
        )

    def close_switch(self):
        """Does the same as `leave_persistent_mode`"""
        self.set_magnet_state(
            types.OiMagnetVectorRotateDemandedState.OiMagnetVectorRotateDemandedStateEnterPersistentMode
        )

    def sweep_small_field_step(self, coord):
        # TODO UNTESTED
        """
        Function sweeps a single VRM group.
        Used for changing the field by values < 100 mA.
        """
        # sweep VRM group
        self.sweep_field()
        # wait until sweep failed
        time.sleep(2)
        status = self.Magnet_State()
        while status != "Holding Not Persistent":
            status = self.Magnet_State()
            time.sleep(1)
        # sweep X, Y or Z group of VRM
        if coord == "X":
            self._param_setter("oi.decs.magnetic_field_control.VRM_01.MAG_X.set_state", 10)
        elif coord == "Y":
            self._param_setter("oi.decs.magnetic_field_control.VRM_01.MAG_Y.set_state", 10)
        elif coord == "Z":
            self._param_setter("oi.decs.magnetic_field_control.VRM_01.MAG_Z.set_state", 10)

    def wait_until_field_stable(self):
        # TODO UNTESTED
        """VRM utility function"""
        time.sleep(2)
        status = self.Magnet_State()
        while status != "Holding Not Persistent":
            status = self.Magnet_State()
            time.sleep(1)
        print(f"Status: {self.Magnet_State()}.")

    def wait_until_field_persistent(self):
        # TODO UNTESTED
        """VRM utility function"""
        status = self.Magnet_State()
        while status != "Holding Persistent":
            status = self.Magnet_State()
            time.sleep(1)
        print(f"Status: {self.Magnet_State()}.")

    def wait_until_field_depersisted(self):
        # TODO UNTESTED
        """VRM utility function"""
        status = self.Magnet_State()
        while status != "Holding Not Persistent":
            status = self.Magnet_State()
            time.sleep(1)
        print(f"Status: {self.Magnet_State()}.")

    def wait_until_temperature_stable_std_control(self, stable_mean, stable_std, time_between_readings):
        # TODO UNTESTED
        """
        Mixing chamber temperature control utility function

        Takes a moving average of 30 temperature readings and finds the mean and the std of the last 30 readings,
        until the difference between the mean and target value is below 'stable_mean' and the standard deviation is below 'stable_std'.

        Args:
            stable_mean: float - difference between the mean and target value to be achieved by the last 30 temperature readings
            stable_std: float - standard deviation to be achieved by the last 30 temperature readings
            time_between_readings: float - time between taking temperature readings

        """
        target_temp = self.Mixing_Chamber_Temperature_Target()

        print(f"Waiting for temperature to stablilise at {target_temp} K.")

        # take 30 temperature readings
        t1 = time.time()
        t_array = np.zeros(30)
        for n in range(0, 30):
            time.sleep(time_between_readings)
            temp = self.Mixing_Chamber_Temperature()
            t_array[n] = float(temp)

        stab = False
        while stab is False:
            time.sleep(time_between_readings)
            temp = self.Mixing_Chamber_Temperature()
            t_array = np.append(t_array, float(temp))

            t_array = t_array[1:]
            s = np.std(t_array)
            m = np.abs(np.mean(t_array) - target_temp)

            if (s < stable_std) and (m < stable_mean):
                stab = True

        t2 = time.time()
        tt = t2 - t1
        print(f"Temperature = {t_array[-1]} K")
        print(f"Temperature stable after {int(tt)} seconds. (Mean-Target = {m} K, StdDev = {s} K)")
